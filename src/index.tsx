import * as React from "react";
import * as ReactDOM from "react-dom";

import { Test } from "./components/test.component"

// TODOS
// 1. Try to switch from electron packager to electron builder
// 2. Try to make main process file from js to ts file 
// 3. Fix security issues (electron)
// 4. simplify main.js file

ReactDOM.render(<Test/>, document.getElementById("mount"));
