const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    target: "electron-renderer",

    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    },
    
    module: {
        rules: [
            {
                test:  /\.tsx?$/,
                loader: "awesome-typescript-loader"
            }
        ]
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.ejs"
        })
    ]
};