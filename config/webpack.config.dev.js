const paths = require("./paths");
const webpack = require("webpack");
const { spawn } = require("child_process");
const merge = require("webpack-merge");
const baseConfig = require("./webpack.config.base");

module.exports = merge(baseConfig, {
    devtool: "cheap-source-map",

    module: {
        rules: [
            {
                enforce: "pre",
                test: /\.tsx?$/,
                loader: "tslint-loader"
            }
        ]
    },

    plugins: [
        new webpack.DefinePlugin({
            "process.env.NODE_ENV": JSON.stringify("development")
        })
    ],

    devServer: {
        contentBase: paths.output,
        before() {
            spawn("electron", ["."], { shell: true, env: process.env, stdio: "inherit" })
                .on("close", (code) => process.exit(0))
                .on("error", (err) => console.error(err));
        }
    }
});
